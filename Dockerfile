FROM ubuntu:20.04

# Install deps
RUN apt update && apt install wget -y

# Create non-root user
RUN groupadd --gid 2000 ubuntu && \
    useradd --uid 2000 \
      --gid ubuntu \
      --shell /bin/bash \
      --create-home ubuntu
USER ubuntu
WORKDIR /home/ubuntu

# Download binaries
RUN wget https://download.iproyal.com/pawns-cli/latest/linux_x86_64/pawns-cli && \
    chmod +x pawns-cli 

# Set entrypoint
ENTRYPOINT [ "./pawns-cli" ]
